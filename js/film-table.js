const films = [
  {
    time: "10:00",
    id: 10,
    title: "Человек-паук",
    genres: ["фантастика", "боевик", "приключения"],
  },
  {
    time: "12:00",
    rate: "R",
    name: "Собачья жизнь 2",
    genres: ["фэнтези", "драма", "комедия"],
  },
  {
    time: "14:00",
    rate: "G",
    name: "История игрушек 4",
    genres: ["мультфильм", "фэнтези", "комедия"],
  },
  {
    id: 12,
    time: "16:00",
    rate: "NC-17",
    name: "Люди в черном: Интернэшнл",
    genres: ["фантастика", "боевик", "комедия"],
  },
  {
    time: "18:00",
    name: "История игрушек 4",
    genres: ["мультфильм", "фэнтези", "комедия"],
  },
  {
    time: "20:00",
    rate: "NC-17",
    name: "Люди в черном: Интернэшнл",
    genres: ["фантастика", "боевик", "комедия"],
  },
  {
    time: "22:00",
    name: "История игрушек 4",
    genres: ["мультфильм", "фэнтези", "комедия"],
  },
  {
    time: "23:00",
    name: "Люди в черном",
    genres: ["фантастика", "боевик", "комедия"],
  },
];

let tbody = document.getElementById("table-body");

tbody.innerHTML = "";

for (const film of films) {
  const filmItem = new Film(film);

  if (filmItem.isNotForAdult()) {
    tbody.innerHTML += filmItem.renderFilmTableItem();
  }
}
