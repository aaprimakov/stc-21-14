function Film(filmData) {
    this._data = filmData;
}

Film.prototype.isNotForAdult = function () {
    return this._data.rate !== "R" && this._data.rate !== "NC-17";
}

Film.prototype.getTitle = function () {
    return this._data.title || this._data.name;
}

Film.prototype.getTime = function () {
    return this._data.time;
}

Film.prototype.getId = function () {
    return this._data.id || `${this._data.name.replaceAll(" ", "-")}-${this._data.time}`;
}

Film.prototype.getGenres = function () {
    return this._data.genres.join(", ");
}

Film.prototype.renderFilmTableItem = function () {
    return `
  <tr class='even'>
      <td >
        <input
          type="checkbox"
          class="block03__checkbox"
          id="${this.getId()}"
        />
        <label for="${this.getId()}">
          <svg
            width=".55rem"
            height=".45rem"
            viewBox="0 0 11 9"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M4.60581 6.79378L1.46056 3.93033L0.787354 4.66979L4.70255 8.23421L10.8223 0.94099L10.0562 0.298203L4.60581 6.79378Z"
            />
          </svg>
        </label>
      </td>
      <td>${this.getTime()}</td>
      <td>${this.getTitle()}</td>
      <td>${this.getGenres()}</td>
  </tr>
    `;
}
