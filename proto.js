function Money(value) {
    this.value = value;
}

function CellItem (cost) {
    this.cost = cost;
    this.bought = false;
}

CellItem.prototype.by = function (money) {
    if (money instanceof Money && money.value >= this.cost) {
        this.bought = true;
        money.value -= this.cost;
    }
}

function Alcohol(alcohol, volume, cost) {
    CellItem.call(this, cost)
    this.alcohol = alcohol;
    this.volume = volume;
}

Alcohol.prototype = Object.create(CellItem.prototype);
Alcohol.prototype.constructor = Alcohol;

Alcohol.prototype.drink = function () {
    if (this.bought) {
        console.log('you drink me');
        this.volume--;
        console.log(`you have left ${this.volume} of me`);
    } else {
        throw new Error('Необходимо куить перед тем как пить')
    }
}

function Beer(alcohol, cost = 3, volume = 22) {
    Alcohol.call(this, alcohol, volume, cost);
    this.cap = true;
}

Beer.prototype = Object.create(Alcohol.prototype);
Beer.prototype.constructor = Beer;

function Wine (alcohol) {
    Alcohol.call(this, alcohol, 7, 100);
    this.сork = true;
}

Wine.prototype = Object.create(Alcohol.prototype);
Wine.prototype.constructor = Wine;

const heiniken = new Beer('4%');
const pit = new Beer('3%');

const tussent = new Wine('3%');
const coins = new Money(1000);

heiniken.by(coins);
heiniken.drink();

try {
    pit.drink();
} catch (error) {
    console.log(error.message);
    
    pit.by(coins);
    pit.drink();
} finally {
    console.log(pit)
}

// tussent.drink();
